#include <openssl/evp.h>
#include <openssl/ec.h>
#include <openssl/ecdh.h>
#include <openssl/err.h>
#include <string.h>
#include <stdio.h>
#include <fcntl.h>
#include <openssl/bn.h>
#include <openssl/bio.h>


int main()
{
	EC_KEY *key1, *key2;
	int field_size;
	unsigned char *secret;
	unsigned char *secret2;
	unsigned char buf[100];
	size_t secret_len;
	int i;
	int len;

	const EC_POINT *ecp;
	const EC_GROUP *ecg;

	unsigned char *pubkey1 = OPENSSL_malloc(65);

	/* Create an Elliptic Curve Key object and set it up to use the ANSI X9.62 Prime 256v1 curve */
	if(NULL == (key1 = EC_KEY_new_by_curve_name(NID_X9_62_prime256v1)))
	{
		printf("key1, new curve_name error\n");
	};

	/* Generate the private and public key */
	if(1 != EC_KEY_generate_key(key1))
	{
		printf("key2 generate key error\n");
	};


	ecg = EC_KEY_get0_group(key1);
	ecp = EC_KEY_get0_public_key(key1);


	/* Get the peer's public key, and provide the peer with our public key -
	 * how this is done will be specific to your circumstances */
	//peerkey = get_peerkey_low(key);

	if(NULL == (key2 = EC_KEY_new_by_curve_name(NID_X9_62_prime256v1)))
	{
		printf("key2, new curve_name error\n");
	};

	/* Generate the private and public key */
	if(1 != EC_KEY_generate_key(key2))
	{
		printf("key2 generate key error\n");
	};

	printf("finish key generate\n");


	/* Calculate the size of the buffer for the shared secret */
	field_size = EC_GROUP_get_degree(EC_KEY_get0_group(key1));

	secret_len = (field_size+7)/8;
	//secret_len = 64;

	/* Allocate the memory for the shared secret */
	if(NULL == (secret = OPENSSL_malloc(secret_len)))
	{
		printf("openssl malloc error\n");
	};

	if(NULL == (secret2 = OPENSSL_malloc(secret_len)))
	{
		printf("openssl malloc error\n");
	};

	/* Derive the shared secret */
	secret_len = ECDH_compute_key(secret, secret_len, EC_KEY_get0_public_key(key2),
			key1, NULL);

	printf("secret_len : %d\n", secret_len);

	secret_len = ECDH_compute_key(secret2, secret_len, EC_KEY_get0_public_key(key1),
			key2, NULL);


	if(strcmp(secret, secret2) == 0)
		printf("match!!\n");


	/* Clean up */
	EC_KEY_free(key1);
	EC_KEY_free(key2);

	if(secret_len <= 0)
	{
		OPENSSL_free(secret);
		return 0;
	}

	return 0;

	//return 0;
}
