CC = gcc
LOCAL_CFLAGS = -g


TEST_APPS = \
		server \
		client \
#

SRCS = \
		 echo_server.c \
		 echo_client.c \
#
		 

all : $(TEST_APPS)
server : $(SRCS)
	$(CC) -g -o $@ echo_server.c
client :
	$(CC) -g -o $@ echo_client.c

clean :
	rm server client
