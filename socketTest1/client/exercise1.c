#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/stat.h>
#include <stdio.h>
#include<string.h>
#include<unistd.h>
#include<stdlib.h>

#define MAXLINE 1024

int main(int argc, char **argv){
	struct sockaddr_in serveraddr;
	int server_sockfd;
	char buf[MAXLINE];
	int client_len;

	if((server_sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1){
		perror("error : ");
		return 1;
	}
		
	serveraddr.sin_family = AF_INET;
	serveraddr.sin_addr.s_addr = inet_addr("127.0.0.1");
	serveraddr.sin_port = htons(atoi(argv[1]));
	client_len = sizeof(serveraddr);
	
	if(connect(server_sockfd, (struct sockaddr *) &serveraddr, client_len) == -1){
		perror("connect error : ");
		return 1;
	}

	while(1){
		memset(buf, 0x00, MAXLINE);
		read(1, buf, MAXLINE);
		write(server_sockfd, buf, MAXLINE);
		read(server_sockfd, buf, MAXLINE);
	}
}
