#include<sys/socket.h>
#include<sys/stat.h>
#include<arpa/inet.h>
#include<stdio.h>
#include<string.h>
#include<unistd.h>
#include<stdlib.h>
#define MAXBUF 1024

int main(int argc, char **argv)
{
	int server_sockfd, client_sockfd;
	struct sockaddr_in serveraddr, clientaddr;
	char buf[MAXBUF]; 
	if((server_sockfd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP ))== -1){
		perror("socket failed");
		return 1;
	}
	int client_len = sizeof(clientaddr);

	bzero(&serveraddr, sizeof(serveraddr));
	serveraddr.sin_family = AF_INET;
	serveraddr.sin_addr.s_addr = htonl(INADDR_ANY);
	serveraddr.sin_port = htons(atoi(argv[1]));

	bind(server_sockfd, (struct sockaddr *) &serveraddr, sizeof(serveraddr));
	listen(server_sockfd, 5);
	printf("listening...\n");

	while(1){
		memset(buf, 0x00, MAXBUF);
		client_sockfd = accept(server_sockfd, (struct sockaddr *)&clientaddr, &client_len);
		printf("new client : %s\n", inet_ntoa(clientaddr.sin_addr));
		read(client_sockfd, buf, MAXBUF);
		printf("buf : %s\n", buf);
		write(client_sockfd, buf, MAXBUF);
	}
}
